## block model generator##
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

##### PARAMS #####
# whether we want a directed network (directed=True) or undirected network (directed=False)
directed = True
# whether we want a homogeneous network (mode=0) or heterogeneous network (mode=1)
mode = 1
# number of blocks
nblocks = 2 # 4
# range for the size of each block (chosen randomly in the range)
size_range = [2,3] # [10,50]
# (approximate) density of the network
density = 0.5 # 0.1
# relative strength of intra-block connections with respect to inter-blocks
# if rel=1 intra- and inter-block connections are equally likely
# if rel>1 intra-block connections are more likely
# if rel<1 inter-block connections are more likely
rel = 2
# whether you want to plot the adjacency matrix
plot = False
##################

sizes = np.random.randint(size_range[0],size_range[1],nblocks)
n = sizes.sum()
intra = (sizes*(sizes-1)/2).sum()
tot = n*(n-1)/2
inter = tot-intra
q = tot/(intra*rel+inter)
p = rel*q
intradensity = p*density
interdensity = q*density

if mode==0:
    pintra = [intradensity for _ in range(n)]
    pinter = [interdensity for _ in range(n)]
else:
    gamma = intradensity/(1-intradensity)
    pintra = np.random.power(gamma,n)
    gamma = interdensity/(1-interdensity)
    pinter = np.random.power(gamma,n)

cs = np.cumsum([0]+sizes.tolist())
adjacency_matrix = np.zeros((n,n))
for j,s in enumerate(sizes):
    for i in range(s):
        pi = pintra[cs[j]+i]
        qi = pinter[cs[j]+i]
        adjacency_matrix[cs[j]+i,cs[j]+i:cs[j+1]] = np.random.choice(2,max(0,cs[j+1]-cs[j]-i),p=(1-pi,pi))
        adjacency_matrix[cs[j]+i,cs[j+1]:] = np.random.choice(2,n-cs[j+1],p=(1-qi,qi))
adjacency_matrix[np.tril_indices(n)] = 0
print(adjacency_matrix)
if not directed:
    adjacency_matrix = adjacency_matrix + adjacency_matrix.T
    L = int(adjacency_matrix.sum()/2)
else:
    L = int(adjacency_matrix.sum())
print('adjacency matrix generated')
print('number of nodes:', n)
print('number of edges:', L)
print('average degree:', L*2/n)
print('density:', L/tot)

basename = f'test_graph_mode_{mode}_nodes_{n}_blocks_{nblocks}_density_{density:.3f}'

# compute degree and number of block2block links
degrees = adjacency_matrix.sum(axis=0)
b2b = []
for i1 in range(len(sizes)):
    b2b.append(adjacency_matrix[cs[i1]:cs[i1+1],cs[i1]:cs[i1+1]].sum()/2)
    for i2 in range(i1+1,len(sizes)):
        b2b.append(adjacency_matrix[cs[i1]:cs[i1+1],cs[i2]:cs[i2+1]].sum())
b2b = np.array(b2b)


print(degrees.sum())
print(b2b.sum())

# write to file 
with open(f'{basename}.csv','w') as fout:
    for j,s in enumerate(sizes):
        for i in range(s):
            print(degrees[cs[j]+i], j, degrees[cs[j]+i]/np.sqrt(2*L), file=fout)
    for k in b2b:
        print(k, k/np.sqrt(2*L), file=fout)
print('data written on:', f'{basename}.csv')
        
if plot:
    sns.heatmap(adjacency_matrix)
    plt.savefig(f'{basename}.png')
    print('adjacency matrix plotted on:', f'{basename}.png')

print('to solve the corresponding system with 0.001 precision and 1000 maxiter run:')
print(f'\tcat {basename}.csv | ./newton {n} {nblocks} 0.001 1000')
