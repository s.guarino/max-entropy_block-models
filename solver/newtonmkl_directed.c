#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <mkl.h>

#define MINARG 5
#define MAXELEMENTS 100000000
#define MAXLINE 1024


double L(int n, int m, double *theta_in, double *theta_out, double *eta, double *k_in, double *k_out, double *K, int *block){
       int i,j,I,J,IJ;
       double result = 0.0;
       double tr;
       int mpairs = m*m;
       for (i=0;i<n;i++){
           I = block[i];
	   tr = 0.;
#pragma omp parallel for private(j,J,IJ) reduction(+:tr)
           for (j=0;j<n;j++){
	       if (j!=i){      
                   J = block[j];
                   IJ = I*m+J;
                   tr += log(1+exp(-theta_out[i]-theta_in[j]-eta[IJ]));
	       }
	   }   
           result += (theta_out[i]*k_out[i]+theta_in[i]*k_in[i]+tr);
       }
#pragma omp parallel for reduction(+:result)
       for (i=0;i<mpairs;i++){
               result += eta[i]*K[i];
       }
       return result;
}


void grad_L(int n, int m, double *theta_in, double *theta_out, double *eta, double *k_in, double *k_out, double *K, int *block, /* gsl_vector */ double *result){
       int i,j,I,J,IJ;
       int mpairs = m*m;
       double x;
       double ri;
#pragma omp parallel for
       for(i=0; i<n; i++) {
	result[i] = k_in[i];
       }
#pragma omp parallel for 
       for(i=n; i<2*n; i++) {
	result[i] = k_out[i-n];
       }
#pragma omp parallel for 
       for(i=2*n; i<(2*n+mpairs); i++) {
	result[i] = K[i-2*n];
       }
       for (i=0;i<n;i++){
           I = block[i];
	   ri=result[n+i];
#pragma omp parallel for private(j,J,IJ,x) reduction(-:ri)
           for (j=0;j<n;j++){
	       if (j!=i){
	           J = block[j];
                   IJ = I*m+J;
                   x = exp(-theta_out[i]-theta_in[j]-eta[IJ]);
	           x = x/(1+x);
	           ri=ri-x;
	           result[j]=result[j]-x;
#pragma omp atomic
	           result[2*n+IJ]=result[2*n+IJ]-x;
	       }
	   }   
	   result[n+i]=ri;
       }
       return;
}


#define mat_get(p,i,j) (*((p)+(dim*(i))+(j)))
#define mat_set(p,i,j,v) *((p)+dim*(i)+(j))=(v)

void hess_L(int n, int m, double *theta_in, double *theta_out, double *eta, int *block, /* gsl_matrix */ double *result){
       size_t i,j,I,J,IJ;
       int mpairs = m*m;
       double x;
       double rii;
       size_t dim=2*n+mpairs, li;
#pragma omp parallel for
       for (li=0;li<(dim*dim);li++){
	    result[li]=0.;
       }
       
       for (i=0;i<n;i++){
           I = block[i];
#pragma omp parallel for private(j,J,IJ,x) reduction(+:rii)
           for (j=0;j<n;j++){
	       if (j!=i){ 
	           J = block[j];
                   IJ = I*m+J;
                   x = exp(-theta_out[i]-theta_in[j]-eta[IJ]);
	           x = x/((1+x)*(1+x));
		   mat_set(result,j,n+i,x);
		   mat_set(result,n+i,j,x);
#pragma omp atomic
	           mat_set(result,n+i,n+i,mat_get(result,n+i,n+i)+x);
#pragma omp atomic
		   mat_set(result,j,j,mat_get(result,j,j)+x);
#pragma omp atomic
	           mat_set(result,n+i,2*n+IJ,mat_get(result,n+i,2*n+IJ)+x);
#pragma omp atomic
	           mat_set(result,2*n+IJ,n+i,mat_get(result,2*n+IJ,n+i)+x);
#pragma omp atomic
	           mat_set(result,j,2*n+IJ,mat_get(result,j,2*n+IJ)+x);
#pragma omp atomic
	           mat_set(result,2*n+IJ,j,mat_get(result,2*n+IJ,j)+x);
#pragma omp atomic
	           mat_set(result,2*n+IJ,2*n+IJ,mat_get(result,2*n+IJ,2*n+IJ)+x);
	       }
	   }   
       }
       return;
}


void init_random(double *x, int n){
    int i;
    for (i=0;i<n;i++){
        x[i] = rand()/RAND_MAX;  
    }
}

void init_alt_1(double *x, int n, double *k, int degs){
    int i;
    double L = 0.0;
    for (i=0;i<n;i++){
        L += k[i];
    }
    if (degs){
        L = L/2;
    }
    for (i=0;i<n;i++){
        x[i] = -log(k[i]/sqrt(2*L)); 
    }
}

void init_alt_2(double *x, int n, double *k){
    int i;
    for (i=0;i<n;i++){
        x[i] = -log(k[i]/sqrt((float)n));
    }
}


void get_expected_degrees(int n, int m, double *theta_in, double *theta_out, double *eta, int *block, double *degrees){
    int i, j, I, J, IJ;
    int mpairs = m*m;
    double num, denom, p, y_IJ;
    memset(degrees, 0, sizeof(double)*(2*n+mpairs)); 
    for (i=0;i<n;i++){
        I = block[i];
        for (j=0;j<n;j++){
	    if (i!=j){
                J = block[j];
                IJ = I*m+J;
                y_IJ = eta[IJ];
                num = exp(-theta_out[i]-theta_in[j]-y_IJ);  
                denom = 1.0+num;  
                p = num/denom;
                degrees[n+i] += p;
                degrees[j] += p;
                degrees[2*n+IJ] += p;
	    }
        }
    }
}
   

double get_made(int n, int mpairs, double *exp_deg, double *k_in, double *k_out, double *K){
    int i; 
    double made=0.0;
    for (i=0;i<n;i++){
        if (fabs(exp_deg[i]-k_in[i])>made){
            made = fabs(exp_deg[i]-k_in[i]);
        }
    }
    for (i=0;i<n;i++){
        if (fabs(exp_deg[n+i]-k_out[i])>made){
            made = fabs(exp_deg[n+i]-k_out[i]);
        }
    }
    for (i=0;i<mpairs;i++){
        if (fabs(exp_deg[2*n+i]-K[i])>made){
            made = fabs(exp_deg[2*n+i]-K[i]);
        }
    }
    return made;
}
   

void print_degrees(int h, int n, int mpairs, double *exp_deg, double *k_in, double *k_out, double *K, FILE *flog){
    int i;
    if (2*n+mpairs<h){
        h = 2*n+mpairs;
    }
    fprintf(flog, "       %8s %8s\n", "k*", "<k>");
    for (i=0;i<h;i++){
        if (i<n){
            fprintf(flog, "%d in: %8.3f %8.3f\n", i, k_in[i], exp_deg[i]);
	}else{
	    if (i<2*n){
                fprintf(flog, "%d out: %8.3f %8.3f\n", i, k_out[i-n], exp_deg[i]);
            }else{
                fprintf(flog, "%d b2b: %8.3f %8.3f\n", i, K[i-2*n], exp_deg[i]);
            }
	}
    }
}


double compute_quality_1(int n, int m, double *u1_in, double *u1_out, double *u2, double *v1, double *v2){
    int i;
    double quality = 0.0;
    for (i=0;i<n;i++){
        quality += (u1_in[i]-u2[i])*(u1_in[i]-u2[i]);
        quality += (u1_out[i]-u2[n+i])*(u1_out[i]-u2[n+i]);
    }
    for (i=0;i<m;i++){
        quality += (v1[i]-v2[i])*(v1[i]-v2[i]);
    }
    quality = sqrt(quality);
    return quality;
}

#if 0
double compute_quality_2(int n, gsl_vector *u){
    int i;
    double temp, quality = 0.0;
    for (i=0;i<n;i++){
        temp = gsl_vector_get(u,i);
        quality += temp*temp;
    }
    quality = sqrt(quality);
    return quality;
}
#endif


int main (int argc, char *argv[]) {
       int sign;
       unsigned int i, j, iter, max_iter, n, m, mpairs;
       size_t dim;
       unsigned int *block;
       char line[MAXLINE];
       double precision, quality, made, temp_in, temp_out, y_IJ, alpha, eps, w, gamma, beta, t, l1, l2;
       double *k_in, *k_out, *K, *theta_in, *theta_out, *eta, *theta_temp, *eta_temp, *exp_deg;
       /*---PARAMS---*/
       eps = 0.001;
       gamma = 0.2;
       beta = 0.5;
       /*------------*/
       FILE *flog = fopen("log.txt", "w");
       fprintf(flog, "solving the system with Newton method\n"); 
       srand(time(NULL));
       if(argc<MINARG) {
	  fprintf(stderr,"Usage %s n_nodes n_blocks precision max_iter\n",argv[0]);
	  exit(1);
       }
       n=atoi(argv[1]);
       m=atoi(argv[2]);
       mpairs = m*m;
       dim = 2*n+mpairs;
       precision=atof(argv[3]);
       max_iter=atoi(argv[4]);
       if(dim>MAXELEMENTS) {
	fprintf(stderr,"Too many elements: max number is %d\n",MAXELEMENTS);
	exit(1);
       }
       theta_in = (double *)malloc(sizeof(double)*n);
       theta_out = (double *)malloc(sizeof(double)*n);
       eta = (double *)malloc(sizeof(double)*mpairs);
       if((theta_in==NULL)||(theta_out==NULL)||(eta==NULL)){
	fprintf(stderr,"Could not get memory for %d items of theta+eta\n",dim);
	exit(1);
       }
       k_in=(double *)malloc(sizeof(double)*n);
       k_out=(double *)malloc(sizeof(double)*n);
       K=(double *)malloc(sizeof(double)*mpairs);
       if((k_in==NULL)||(k_out==NULL)||(K==NULL)){
	fprintf(stderr,"Could not get memory for %d items of k+K\n",dim);
	exit(1);
       }
       block=(unsigned int *)malloc(sizeof(unsigned int)*n);
       if(block==NULL) {
	fprintf(stderr,"Could not get memory for %d items of block\n",n);
	exit(1);
       }
       for(i=0; i<n; i++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i);
		exit(1);
	}
	sscanf(line,"%lf %lf %d %lf %lf",k_in+i,k_out+i,block+i,&temp_in,&temp_out);
	if (temp_in==0.0){
	    temp_in = 0.001;
	}  
	theta_in[i] = -log(temp_in);
	if (temp_out==0.0){
            // printf("WARNING: initial value for exp(theta_out[%d]) is 0! will be set to 0.001\n", i);
	    temp_out = 0.001;
	}  
	theta_out[i] = -log(temp_out);
       }
       for(j=0; j<mpairs; j++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i+j);
		exit(1);
	}
	sscanf(line,"%lf %lf",K+j,&temp_in);
	if (temp_in==0.0){
            // printf("WARNING: initial value for exp(eta[%d]) is 0! will be set to 0.001\n", j);
	    temp_in = 0.001;
	}  
	eta[j] = -log(temp_in);
       }
       
       /* alternative initializations */
       // init_random(eta, n);
       // init_random(eta, mpairs);
       // init_alt_1(theta, n, k, 1);
       // init_alt_1(eta, mpairs, K, 0);
       // init_alt_2(theta, n, k);
       // init_alt_2(eta, mpairs, K);
       
       double *x, *dl;

       dl=malloc(sizeof(double)*dim);
       if((dl==NULL)){
	fprintf(stderr,"Could not get memory for %d items of dl\n",dl);
	exit(1);
       }

       exp_deg = (double *)malloc(sizeof(double)*(dim));
       theta_temp = (double *)malloc(sizeof(double)*(2*n));
       eta_temp = (double *)malloc(sizeof(double)*mpairs);
       iter = 0;
       MKL_INT nmkl = dim, nrhs = 1, lda = dim, ldb = dim, info;       
       /* start iterating */
       MKL_INT perm[dim];
       double *Hl;
       double *xsol;
       xsol=malloc(sizeof(double)*dim);
       if((xsol==NULL)){
	fprintf(stderr,"Could not get memory for %d items of x\n",dim);
	exit(1);
       }
       x=xsol;
       Hl=malloc(sizeof(double)*(dim*dim));
       if((Hl==NULL)){
	fprintf(stderr,"Could not get memory for %d items of Hl\n",dim*dim);
	exit(1);
       }

       do {
           iter++;
           // printf("At iter %d: starting gradient evaluation\n", iter);
           grad_L(n, m, theta_in, theta_out, eta, k_in, k_out, K, block, dl);
	   for(i=0; i<dim; i++) {
	          dl[i]*=(-1.0);
	   }
           // printf("At iter %d: starting hessian evaluation\n", iter);
	   hess_L(n, m, theta_in, theta_out, eta, block, Hl);
	   for(i=0; i<dim; i++) { Hl[(i*dim)+i]+=eps; }
           // printf("At iter %d: starting linear system resolution\n", iter);
	   /* LU decomposition and forward&backward substition */
	   memcpy(xsol,dl,dim*sizeof(double));
	   dgesv( &nmkl, &nrhs, Hl, &lda, perm, xsol, &ldb, &info );
           // printf("At iter %d: linear system resolution completed!\n", iter);
	   /* select proper alpha */
	   alpha = 1.0;
	   t=cblas_ddot(dim, dl, 1, xsol, 1);	   
	   t = -t;
	   l1 = L(n, m, theta_in, theta_out, eta, k_in, k_out, K, block);
           for (i=0;i<n;i++){
	       theta_temp[i] = theta_in[i];
	       theta_temp[n+i] = theta_out[i];
	   }
           for (i=0;i<mpairs;i++){
	       eta_temp[i] = eta[i];
	   }
	   // printf("At iter %d: starting loop\n", iter);
	   while (0==0) {
               for (i=0;i<n;i++){
		   theta_in[i] = theta_temp[i]+alpha*x[i];		 
		   theta_out[i] = theta_temp[n+i]+alpha*x[n+i];		 
	       }
               for (i=0;i<mpairs;i++){
		   eta[i] = eta_temp[i]+alpha*x[2*n+i];		 
	       }
	       l2 = L(n, m, theta_in, theta_out, eta, k_in, k_out, K, block);
	       if (l2>=l1+gamma*alpha*t){
	           alpha = beta*alpha;
	       }else{
	           break;
	       }
	   }	   
           // printf("At iter %d: end of loop\n", iter);
           quality = compute_quality_1(n,mpairs,theta_in,theta_out,theta_temp,eta,eta_temp);
           // quality = compute_quality_2(dim,dl);
           /* compute expected degrees based on the obtained values */
           get_expected_degrees(n, m, theta_in, theta_out, eta, block, exp_deg);
	   /* compute maximum absolute degree error */
	   made = get_made(n, mpairs, exp_deg, k_in, k_out, K);
	   // fprintf(flog, "At iter %d: precision=%13.11f, MADE=%13.11f\n", iter, quality, made);
       } while (made>=1 && iter<max_iter);
       // } while (quality>=precision && iter<max_iter);
           
       /* compute expected degrees based on the obtained values */
       get_expected_degrees(n, m, theta_in, theta_out, eta, block, exp_deg);
       /* compute maximum absolute degree error */
       made = get_made(n, mpairs, exp_deg, k_in, k_out, K);
       fprintf(flog, "Solver converged after %d iterations!\nFinal precision=%13.11f, final MADE=%13.11f\n", iter, quality, made);
       // printf("Solver converged after %d iterations!\nFinal precision=%13.11f, final MADE=%13.11f\n", iter, quality, made);
       /* imposed and expected degree all vertices of the network */
       print_degrees(2*n+mpairs, n, mpairs, exp_deg, k_in, k_out, K, flog);
#if 1      
       for (i=0;i<n;i++){
           printf("%g\n", exp(-theta_in[i]));
       }
       for (i=0;i<n;i++){
           printf("%g\n", exp(-theta_out[i]));
       }
       for (i=0;i<mpairs;i++){
           printf("%g\n", exp(-eta[i]));
       }
#endif       
       /* free all */
       free(Hl);
       free(xsol);
       free(dl);

       fclose(flog);
       return 0;
}
