import matplotlib.pyplot as plt
import numpy as np
import powerlaw
from utils import print_time, find_xmin_from_expected_value, expected_measures

def plot_log_distribution(data, alpha = None, xm = None, pwfit = None, grid = True, title_str = None, data_str = 'Data', data_color = 'blue', ax = None, double_log = True):
    max = int(np.max(data)+2)
    hist = []
    if len(data.shape) > 1: # if I've got more than one measurement
        for datum in data:
            this_hist, bin_edges = np.histogram(datum, bins = [n for n in range(1,max)], density = True)
            hist.append(list(this_hist))
    else:
        this_hist, bin_edges = np.histogram(data, bins = [n for n in range(1,max)], density = True)
        hist.append(list(this_hist))

    hist_avg = np.mean(hist, axis = 0)
    hist_std = np.std(hist, axis = 0)
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot()
    else:
        fig = ax.get_figure()
    ax.errorbar(bin_edges[:-1], hist_avg, yerr=hist_std, label = data_str, color = data_color, fmt = 'o')
    ax.set_xscale('log')
    if double_log: ax.set_yscale('log')
    if title_str is not None:
        ax.set_title(title_str)

    if alpha is not None and xm is not None:
        x_data = np.logspace(np.log10(xm),np.log10(max),num = 20)
        y = lambda x: np.power(xm * (alpha-2) / x, alpha-1) / x
        y_data = y(x_data)
        ax.plot(x_data, y_data, color = 'red', label = 'Powerlaw distribution' + '\n' + r'$\alpha =$'+f'{alpha:.2f}' + '\n' + r'$x_m=$' + f'{xm:.2f}')
    if pwfit is not None:
        xm = pwfit.power_law.xmin
        alpha = pwfit.power_law.alpha
        x_data = np.logspace(np.log10(xm),np.log10(max),num = 20)
        y = lambda x: np.power(xm * (alpha-2) / x, alpha-1) / x
        y_data = y(x_data)
        ax.plot(x_data, y_data, color = 'green', label = 'Powerlaw fit' + '\n' + r'$\alpha =$'+f'{alpha:.2f}' + '\n' + r'$x_m=$' + f'{xm:.2f}')
    if grid:
        ax.grid(True)
    ax.legend()
    return fig, ax

def measure_fitness(fitness, avg_deg, alpha, path, xmin = None):
    # Fit exponents with power law
    results = powerlaw.Fit(fitness.ravel())
    print_time(f'Measured exponent of the fitness: {results.power_law.alpha}')
    print_time(f'Expected exponent of the fitness: {alpha}')
    if alpha is not None: print_time(f'Relative error: {(alpha - results.power_law.alpha) / alpha * 100:.4f}%')
    print_time(f'Measured average: {np.mean(fitness)}')
    print_time(f'Expected average: {avg_deg}')
    print_time(f'Relative error: {(avg_deg - np.mean(fitness)) / avg_deg * 100:.4f}%')
    print_time(f'Measured x min of the fitness: {results.power_law.xmin}')
    if bool(xmin):
        xmin = find_xmin_from_expected_value(avg_deg, alpha)
        print_time(f'Expected x min of the fitness: {xmin}')
        print_time(f'Relative error: {(xmin - results.power_law.xmin) / xmin * 100:.4f}%')


    fig, _ = plot_log_distribution(fitness, alpha = alpha, xm = xmin, pwfit = results, title_str = 'Fitness distribution')
    fig.savefig(path)

def measure_degree(degree, avg_deg, alpha, path, xmin = None):
    # Fit exponents with power law
    results = powerlaw.Fit(degree.ravel())
    print_time(f'Measured average: {np.mean(degree)}')
    print_time(f'Expected average: {avg_deg}')
    print_time(f'Relative error: {(avg_deg - np.mean(degree)) / avg_deg * 100:.4f}%')
    print_time(f'Measured x min of the degree distribution: {results.power_law.xmin}')
    if bool(xmin):
        xmin = find_xmin_from_expected_value(avg_deg, alpha)
        print_time(f'Expected x min of the  degree distribution: {xmin}')
        print_time(f'Relative error: {(xmin - results.power_law.xmin) / xmin * 100:.4f}%')


    fig, ax = plot_log_distribution(degree, alpha = alpha, xm = xmin, pwfit = results, title_str = 'Degree distribution')
    ax.axvline(avg_deg, label = f'Expected average {avg_deg:.2f}', color = 'red', ls='--')
    ax.axvline(np.mean(degree), label = f'Measured average {np.mean(degree):.2f}', color = 'black', ls='--')
    ax.legend()
    fig.savefig(path)

def fitness_vs_degree(degree, fitness, path):
    fig, axs = plt.subplots(1,2, figsize = (16,5))
    fig, axs[0] = plot_log_distribution(degree, title_str = 'Fitness and degree distribution', ax = axs[0], data_color = 'cyan', data_str = 'Degree Data', grid = True)
    fig, axs[0] = plot_log_distribution(fitness, ax = axs[0], data_color = 'blue', data_str = 'Fitness Data')
    fitness_degree_relative_error = (degree - fitness) / fitness
    axs[1].scatter(fitness, fitness_degree_relative_error, color = 'blue')
    axs[1].set_title('Degree - Fitness relative error distribution')
    axs[1].set_xscale('log')
    axs[1].grid()
    print(f'Degree - Fitness: Mean absolute error = {np.mean(np.abs((degree - fitness))):.4f}')
    fig.savefig(path)

def measure_K_sim(p, communities, communities_names):
    n_communities = len(communities_names)
    K_sim = np.zeros((n_communities, n_communities))
    for i, comm_i in enumerate(communities_names):
        comm_i_indices = communities[comm_i]
        for j, comm_j in enumerate(communities_names):
            comm_j_indices = communities[comm_j]
            rows = p[comm_i_indices,:]
            rows_and_cols = rows[:,comm_j_indices]
            K_sim[i,j] += np.sum(rows_and_cols.ravel())
    return K_sim

def K_relative_error(K, K_sim):
    if len(K_sim.shape) == 3:
        K_sim_avg = np.mean(K_sim, axis = 0)
        relative_error_K = (K_sim_avg - K) / K_sim_avg
    else:
        relative_error_K = (K_sim - K) / K_sim
    relative_error_K = np.nan_to_num(relative_error_K, nan = 0)
    return relative_error_K

def K_standard_devs(K, K_sim):
    if len(K_sim.shape) == 3:
        K_sim_avg = np.mean(K_sim, axis = 0)
        K_sim_std = np.std(K_sim, axis = 0)
        relative_error_K = (K_sim_avg - K) / K_sim_std
    else:
        relative_error_K = (K_sim - K) / K
    relative_error_K = np.nan_to_num(relative_error_K, nan = 0)
    return relative_error_K

def matrix_heatmap(matrix, communities_names, path, matrix_text = None, vlim = (-0.1, 0.1)):
    fig, ax = plt.subplots(figsize=(6, 5))
    vmin, vmax = vlim
    # colormap
    cmap = 'coolwarm'
    # Plot heatmaps
    im1 = ax.imshow(matrix, cmap=cmap, aspect='auto', interpolation='none', vmin = vmin, vmax = vmax)

    # Set titles
    ax.set_title('Relative error matrix')

    # Add color bar
    cbar = fig.colorbar(im1, ax=ax, orientation='vertical')
    cbar.set_label('Relative error')

    if matrix_text is None:
        matrix_text = [[f'{matrix[i, j]:.2f}' for j in range(matrix.shape[1])] for i in range(matrix.shape[0])]

    # Show values in the heatmap boxes
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            ax.text(j, i, matrix_text[i][j], ha='center', va='center', color='w')
    

    ax.set_xticks(np.arange(len(communities_names)), labels=communities_names)
    ax.set_yticks(np.arange(len(communities_names)), labels=communities_names)
    fig.savefig(path)
    return fig, ax

def edges_error(A, N_blocks, delta, avg_deg):
    _, E, _ = expected_measures(N_blocks, delta, avg_deg)
    E_sim = np.sum(A) / 2
    print(f"Expected edges: {E}")
    print(f"Edges simulation: {E_sim}")
    print(f"Relative error edges: {(E_sim - E) / E * 100}%")

import matplotlib.ticker as ticker

def plot_heatmaps(matrix1, matrix2, path):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 5), gridspec_kw={'width_ratios': [1, 1]})

    # Using 'plasma' colormap
    cmap = 'YlGn'

    # Check if the matrix contains multiple measurements
    if len(matrix2.shape) == 3:
        matrix2_avg = np.mean(matrix2, axis = 0)
        matrix2_std = np.std(matrix2, axis = 0)

    else:
        matrix2_avg = matrix2
        matrix2_std = np.zeros(matrix2.shape) 

    # Plot heatmaps
    im1 = ax1.imshow(matrix1, cmap=cmap, aspect='auto', interpolation='none')
    im2 = ax2.imshow(matrix2_avg, cmap=cmap, aspect='auto', interpolation='none')

    # Set titles
    ax1.set_title('Input matrix')
    ax2.set_title('Output from simulation')

    # Add color bar
    cbar = fig.colorbar(im1, ax=[ax1, ax2], orientation='horizontal', location = 'bottom', aspect = 60)
    cbar.set_label('Value')

    # Show values in the heatmap boxes
    for i in range(matrix1.shape[0]):
        for j in range(matrix1.shape[1]):
            ax1.text(j, i, f'{matrix1[i, j]:.2f}', ha='center', va='center', color='black')

    for i in range(matrix2_avg.shape[0]):
        for j in range(matrix2_avg.shape[1]):
            ax2.text(j, i, f'{matrix2_avg[i, j]:.2f}\n({matrix2_std[i, j]:.2f})', ha='center', va='center', color='black', size = 7)

    # Adjust layout
    ax1.xaxis.set_major_locator(ticker.NullLocator())
    ax1.yaxis.set_major_locator(ticker.NullLocator())
    ax2.xaxis.set_major_locator(ticker.NullLocator())
    ax2.yaxis.set_major_locator(ticker.NullLocator())

    fig.savefig(path)
    return

from matplotlib.pyplot import cm
def degree_distribution(degree, communities, path):
    n_communities = len(communities.keys())
    fig, axs = plt.subplots(1, 1, figsize=(8, 6))
    color = cm.rainbow(np.linspace(0, 1, n_communities))
    for community_id, (community, node_indices) in enumerate(communities.items()):
        stubs_block = degree[node_indices]
        mean = np.mean(stubs_block)
        plot_log_distribution(stubs_block, data_str = f'Degree group {community}', ax = axs, data_color = color[community_id])
        axs.axvline(mean, label = f'Mean degree group {community_id}: {mean:.2f}', c = color[community_id])
        axs.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        # plot_log_distribution(stubs_block, data_str = f'Degree group {group_id}', data_color = 'blue', ax = axs[group_id])
        # axs[group_id].axvline(mean, label = f'Mean degree group {group_id}: {mean:.2f}', color = 'red')
        # axs[group_id].legend(loc='center left', bbox_to_anchor=(1, 0.5))

    fig.suptitle("Degree distributions per group")
    fig.savefig(path)

def get_dist(data):
    data = sorted(data)
    xdata = []
    ydata = []
    x = data[0]
    y = 1
    i = 1
    l = len(data)
    while(i<l):
        xnew = data[i]
        if xnew == x:
            y += 1
        else:
            xdata.append(x)
            ydata.append(y/l)
            x = xnew
            y = 1
        i += 1
    xdata.append(x)
    ydata.append(y/l)
    return xdata, ydata


def get_fit_dist(alpha, xmin, xmax, scale):
    xfit = np.linspace(xmin,xmax,num=100)
    beta = (alpha-1)/(xmin**(1-alpha)-xmax**(1-alpha)) * scale
    yfit = beta*xfit**(-alpha)
    return xfit, yfit


def fit_data(data, xmin=None, xmax=None, compare=False, plot=False, discrete=True, output_file='', verbose=False):
    ''' fit data to a power law

        Args:
            data            is the distribution to fit, can be either a list or a dictionary
                            if a list, is interpreted as the list of all outcomes (e.g., [0,1,2,1,1] means pr(0)=pr(2)=0.2, pr(1)=0.6)
                            if a dictionary, items are interpreted as outcome:number_of_occurrences (e.g., {0:1, 1:3, 2:1})
            xmin
            xmax
            compare         another distribution to which the power law fit can be compared; if True or invalid, exponential is used
            plot            specifies whether the fit must be plotted (both pdf fit and ccdf fit are plotted)
            output_file     the output file for the plot; if empty a default name is used
    '''
    if isinstance(data,dict):
        d = []
        for k,v in data.items():
            d += [k]*v
        data = np.array(d)
    else:
        data = np.array(data)

    fit = powerlaw.Fit(data=data, discrete=discrete, xmin=xmin, xmax=xmax, linear_bins=True)
    if verbose:
        print('Power-law best fit to data:')
        print('\t alpha = {}'.format(fit.power_law.alpha))
        print('\t sigma = {}'.format(fit.power_law.sigma))
        print('\t xmin = {}'.format(fit.power_law.xmin))
        print('\t xmax = {}'.format(fit.power_law.xmax))
    if compare:
        if compare == True:
            R, p = fit.distribution_compare('power_law', 'exponential', normalized_ratio = True)
        else:
            try:
                R, p = fit.distribution_compare('power_law', compare, normalized_ratio=True)
            except:
                print('the argument to option "compare" is not a valid distribution, will use exponential')
                R, p = fit.distribution_compare('power_law', 'exponential', normalized_ratio = True)
        if verbose:
            print('Normalized loglikelihood ratio: {}'.format(R))
            print('Significance value: {}'.format(p))
    if plot:
        fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(10,10))
        fit.plot_pdf(color= 'b', ax=ax, label='data pdf')
        fit.power_law.plot_pdf(color= 'b',linestyle='--',label='fit pdf', ax=ax)
        fit.plot_ccdf(color='r', ax=ax, label='data ccdf')
        fit.power_law.plot_ccdf(color='r', linestyle='--', ax=ax, label='fit ccdf')
        plt.legend()
        if not output_file:
            output_file = 'power_law_fit.png'
        fig.savefig(output_file)
    return fit

def plot_degree_distribution_with_fit(data, plot_to=None):

    fig = plt.figure(figsize=(10,8))
    ax = fig.add_subplot(111)
    ax.set_xscale('log')
    ax.set_yscale('log')

    for k,d in data.items():
        #### scatter plot of data distribution ####
        d = np.array(d)
        xdata, ydata = get_dist(d)

        if xdata[0]==0:
            prob0 = '={:.3f})'.format(ydata[0])
        else:
            prob0 = '=0)'

        ax.scatter(xdata, ydata, s=80, alpha=0.4, label=f'{k} (' + r'$\Pr[0]$' + prob0)
        ymindata = min(ydata)

        fit = fit_data(d, discrete=True, compare=True, verbose=False)
        alpha = fit.alpha
        sigma = fit.sigma
        D     = fit.D
        xmin  = fit.xmin
        xmax = xdata[-1]
        scale = sum(ydata[xdata.index(xmin):xdata.index(xmax)])
        xfit, yfit = get_fit_dist(alpha, xmin, xmax, scale)

        xfit = xfit[yfit>=ymindata]
        yfit = yfit[yfit>=ymindata]
        ax.plot(xfit, yfit, '--', label=f'PL fit of {k} (' + r'$\alpha$' +'={:.2f})'.format(alpha))

        print("\n")
        print("-"*80)
        print(f"FIT DATA for {k}:")
        print("-"*80)
        print("alpha = {}, sigma = {}, D = {}".format(alpha, sigma, D))
        print("xmin = {}, xmax = {}".format(xmin, xmax))
        print("-"*80)
        print("\n")

    title = 'degree distribution'
    ax.set_title(title, fontsize=10)
    ax.legend(loc='upper right', prop={'size': 12})
    ax.tick_params(labelsize=18)
    ax.set_ylabel('Probability', labelpad=40, fontsize=20)

    if plot_to is None:
        outfile = 'degree_distribution.png'
    else:
        outfile = plot_to
    fig.savefig(outfile, bbox_inches='tight') #, dpi=150)
    print("plot_distribution_with_fit: plot saved to {}".format(outfile))

    return
