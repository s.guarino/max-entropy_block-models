from utils import *
import argparse
import os
import inquirer

from utils_measurements import *

if os.name == 'nt':
    dir_str = '\\'
else:
    dir_str = '/'

def main(config_path):
    setting_data = load_config_file(config_path, way = 'measurements')
    if bool(setting_data.get('save_timestamp')):
        input_directory = setting_data['output_directory'].split(dir_str)[:-1]
        input_directory = dir_str.join(input_directory)
    else:
        input_directory = setting_data['output_directory']
    subfolders_exists = contains_subdirectory(input_directory)
    if subfolders_exists:
        subdirs = [walker[0] for walker in os.walk(input_directory)]
        questions = [
            inquirer.List('subdir',
                message='Pick a subdirectory',
                choices=subdirs
            ),
        ]
        answer = inquirer.prompt(questions)
        subdir = answer['subdir']
        input_directory = os.path.join(input_directory, subdir)
        output_directory = os.path.join(input_directory, 'measurements')
    else:
        output_directory = os.path.join(input_directory, 'measurements')

    create_folder(output_directory, setting_data['verbose'])
    input_path = os.path.join(input_directory, setting_data['input_filename'])
    if (os.path.exists(input_path)) is False:
        raise FileExistsError(f"Input file {input_path} does not exist")
    input_data = np.load(input_path, allow_pickle=True).item()
    n_tests = input_data['n_tests']
    communities = input_data['communities']
    communities_names = input_data['communities_names']
    delta = input_data['delta']
    N, E, K = expected_measures(communities, delta, input_data['avg_deg'])
    degrees_sim = []
    K_sim = []
    fitness_sim = []
    if bool(n_tests):
        for test_id in range(n_tests):
            test_id = test_id
            fitness_file = add_test_id(setting_data['fitness_filename'], test_id + 1)
            theta_file = add_test_id(setting_data['theta_filename'], test_id + 1)
            p_ij_file = add_test_id(setting_data['probability_filename'], test_id + 1)
            fitness_path = os.path.join(input_directory, fitness_file)
            theta_path = os.path.join(input_directory, theta_file)
            p_ij_path = os.path.join(input_directory, p_ij_file)

            if (os.path.exists(fitness_path) and os.path.exists(theta_path) and os.path.exists(p_ij_path)) is False:
                raise FileExistsError(f"Output files {fitness_path}, {theta_path} and {p_ij_path} do not exist")
            fitness_this_sim = np.load(fitness_path)
            # theta = np.load(theta_path) Useless at the moment
            p_ij = load_triu(p_ij_path)

            degrees_this_sim = np.sum(p_ij, axis = 1)
            K_this_sim = measure_K_sim(p_ij, communities, communities_names)
            degrees_sim.append(list(degrees_this_sim))
            K_sim.append(list(K_this_sim))
            fitness_sim.append(list(fitness_this_sim))
    else:
        fitness_file = setting_data['fitness_filename']
        theta_file = setting_data['theta_filename']
        p_ij_file = setting_data['probability_filename']
        fitness_path = os.path.join(input_directory, fitness_file)
        theta_path = os.path.join(input_directory, theta_file)
        p_ij_path = os.path.join(input_directory, p_ij_file)

        if (os.path.exists(fitness_path) and os.path.exists(theta_path) and os.path.exists(p_ij_path)) is False:
            raise FileExistsError(f"Output files {fitness_path}, {theta_path} and {p_ij_path} do not exist")
        fitness_this_sim = np.load(fitness_path)
        # theta = np.load(theta_path) Useless at the moment
        p_ij = load_triu(p_ij_path)

        degrees_this_sim = np.sum(p_ij, axis = 1)
        K_this_sim = measure_K_sim(p_ij, communities, communities_names)
        degrees_sim.append(list(degrees_this_sim))
        K_sim.append(list(K_this_sim))
        fitness_sim.append(list(fitness_this_sim))

    degrees_sim = np.array(degrees_sim)
    K_sim = np.array(K_sim)
    fitness_sim = np.array(fitness_sim)
    degree_path = os.path.join(output_directory, 'degree.png')
    measure_degree(degrees_sim, input_data.get('avg_deg'), input_data.get('alpha'), degree_path, input_data.get('xmin'))
    fitness_path = os.path.join(output_directory, 'fitness.png')
    measure_fitness(fitness_sim, input_data.get('avg_deg'), input_data.get('alpha'), fitness_path, input_data.get('xmin'))
    K_path = os.path.join(output_directory, 'K.png')
    plot_heatmaps(K, K_sim, K_path)
    K_err_path = os.path.join(output_directory, 'K_err.png')
    K_std_path = os.path.join(output_directory, 'K_std.png')
    K_err = K_relative_error(K, K_sim)
    K_std = K_standard_devs(K, K_sim)
    matrix_heatmap(K_err, communities_names, K_err_path)
    matrix_heatmap(K_std, communities_names, K_std_path, vlim = (None, None))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, default='config.py', help='path to config file')
    args = parser.parse_args()
    main(args.config)