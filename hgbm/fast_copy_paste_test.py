test = {
# 1
    "communities": {"0": [n for n in range(0,5000)]},
    "delta": [[2]],

# 2
    "communities": {"0": [n for n in range(0,2500)], "1": [n for n in range(2500,5000)]},
    "delta": [[1.2, 1], [1, 1.2]],
    "delta": [[3, 1], [1, 3]],

# 4
    "communities": {"0": [n for n in range(0,1250)], "1": [n for n in range(1250,2500)], "2": [n for n in range(2500,3750)], "3": [n for n in range(3750,5000)]},
    "delta": [[1.2, 1, 1, 1], [1, 1.2, 1, 1], [1, 1, 1.2, 1], [1, 1, 1, 1.2]],
    "delta": [[3, 1, 1, 1], [1, 3, 1, 1], [1, 1, 3, 1], [1, 1, 1, 3]],
}