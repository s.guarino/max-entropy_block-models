import numpy as np

config = {
    "communities": {"0": [n for n in range(0,5000)], "1": [n for n in range(5000,10000)]},
    "delta": [[3, 1], [1, 3]],
    "degrees": None,
    "beta": 10.0,
    "alpha": 2.5,
    "avg_deg": 10.0,
    "xmin": None,
    "verbose": True,
    "output_directory": "/home/davide/AI/Projects/HGBM/results/5000_4_strong",
    "save_timestamp": False,
    "n_tests": None
}