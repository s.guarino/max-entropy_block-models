import numpy as np
import powerlaw
from datetime import datetime
import importlib.util
import os

BETA_DEFAULT = 10
ALPHA_DEFAULT = 2.5
PROBABILITY_FILENAME_DEFAULT = "hgbm.npy"
THETA_FILENAME_DEFAULT = "theta.npy"
FITNESS_FILENAME_DEFAULT = "fitness.npy"
INPUT_FILENAME_DEFAULT = "input.npy"
FOLDER_DEFAULT = "./"

def create_folder(path, verbose):
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
        if verbose: print_time(f"Output directory created: {path}")
    else:
        if verbose: print_time(f"Output directory {path} already exists")
    return None
def add_test_id(filename, test_id = None):
    if test_id is None:
        return filename
    else:
        basename = os.path.basename(filename)
        extention = os.path.splitext(basename)[1]
        return f"{basename.split('.')[0]}_{test_id}{extention}"
def print_time(string, begin = None):
    if begin is None:
        time = datetime.now()
        print(f"{time.strftime('%Y-%m-%d %H:%M:%S')}: {string}")
    else:
        print(f"{begin}{string}")

def save_triu(path, A):
    # Get the indices of the upper triangle
    triu_indices = np.triu_indices_from(A)

    # Extract the upper triangle values
    upper_triangle_values = A[triu_indices]

    # Save the upper triangle values
    np.save(path, upper_triangle_values)
    print_time(f"Proability matrix saved in {path}")

def load_triu(path):
    # Load the upper triangle values
    upper_triangle_values = np.load(path)

    # Reconstruct the original matrix
    N = int(np.sqrt(upper_triangle_values.size * 2))  # Determine the size of the original matrix
    reconstructed_matrix = np.zeros((N, N))

    # Get the indices of the upper triangle
    triu_indices = np.triu_indices(N)

    # Place the values back into the reconstructed matrix
    reconstructed_matrix[triu_indices] = upper_triangle_values

    # Since the matrix is symmetrical, copy the upper triangle to the lower triangle
    reconstructed_matrix = reconstructed_matrix + reconstructed_matrix.T - np.diag(np.diag(reconstructed_matrix))
    return reconstructed_matrix

def contains_subdirectory(directory):
    for _, dirs, _ in os.walk(directory):
        if dirs:
            return True
    return False

def load_config_file(path, way = 'experiments'):
    config = importlib.util.spec_from_file_location("config", path).loader.load_module()
    input_data = config.config
    input_data_formatted = check_input_format(input_data, way)
    return input_data_formatted

def check_data_concistencies(communities, delta, verbose = True):
  if delta.shape[0] != delta.shape[1]:
    raise ValueError('Mixing matrix delta must be a square matrix.')
  if np.any(delta.T != delta):
    raise ValueError('Mixing matrix delta must be symmetrical.')

  if type(communities) == dict:
    if verbose: print("Communities are dictionaries: each key contains the indices of the nodes in that community")
    n_communities = len(communities.keys())
    if verbose: print(n_communities, "communities found")
  elif type(communities) == np.ndarray:
    if verbose: print("Communities are lists: each element contains the total number of nodes in that community")
    n_communities = communities.shape[0]
  else:
      ValueError("communities {type(communities)} not implemented. Aborting")
  if n_communities != delta.shape[0]:
    raise ValueError('Inconcistency between the number of communities in n_communities and delta.')
  


def get_N(communities):
    return np.sum([np.sum(len(comm)) for comm in communities.values()])
def get_E(N, avg_deg):
    return N * avg_deg / 2
def get_K(delta, E):
    proportions = delta / np.sum(delta)
    print("Normalized Delta:")
    print(proportions * 2)
    K = proportions * 2 * E
    return K

def expected_measures(communities, delta, avg_deg):

    N = get_N(communities)
    E = get_E(N, avg_deg)
    K = get_K(delta, E)

    return N, E, K

def get_node_index_to_community(N, communities, communities_names):
    community_to_integer = {comm: i for i, comm in enumerate(communities_names)}
    node_to_communities_int = np.zeros(N, dtype = int)
    for community, indices in communities.items():
        for index in indices:
            node_to_communities_int[index] = community_to_integer[community]
    node_to_communities_int = np.array(node_to_communities_int, dtype = int)
    return node_to_communities_int
#############
#
# Samplings
#
#############

def sample_angle(N):
    return np.random.uniform(0, 2 * np.pi, N)

def find_xmin_from_expected_value(exp_value, alpha):
    return (alpha - 2) / (alpha - 1) * exp_value

def sample_latent_coordinate_powerlaw(N, avg_deg, alpha, xmin = None):
  """
  Sampling the latent coordinate with power law distribution.
  """
  if xmin is None: xmin = find_xmin_from_expected_value(avg_deg, alpha)
  theoretical_distribution = powerlaw.Power_Law(xmin=xmin, parameters=[alpha])
  return theoretical_distribution.generate_random(N)

def get_R(N):
  return N / (2 * np.pi)

def delta_theta_ij_vectorized(R, theta):
    theta_i = theta[:, np.newaxis]
    theta_j = theta[np.newaxis, :]
    x_ij = (np.pi - np.abs(np.pi - np.abs(theta_i - theta_j))) * R
    return x_ij

def get_p_ij_vectorized(fitness, theta, beta, N, K, sum_block_fitness, node_index_to_block):

    # Expanded to all pairs
    I = node_index_to_block[:, np.newaxis] # column vec
    J = node_index_to_block[np.newaxis, :] # row vec

    # warning: make sure that node_index_to_block is an integer array!
    sum_f_u_j = sum_block_fitness[J]
    sum_f_u_i = sum_block_fitness[I]

    K_IJ = K[I, J]
    R = get_R(N)
    x_ij = delta_theta_ij_vectorized(R, theta)

    f_i = fitness[:, np.newaxis]  # Convert to column vector
    f_j = fitness[np.newaxis, :]  # Convert to row vector

    # Calculate denominator terms    
    den_den = (f_i * f_j) / (sum_f_u_i * sum_f_u_j) * beta * (np.sin(np.pi / beta) / (2 * np.pi)) * N * K_IJ
    t = x_ij / den_den

    p_ij = 1 / (1 + np.power(t, beta))
    np.fill_diagonal(p_ij, 0)
    return p_ij

def get_fitness(N, alpha, avg_deg, degrees, xmin):
    if degrees is not None:
       return degrees
    else:
       return sample_latent_coordinate_powerlaw(N, avg_deg, alpha, xmin)
#############
#
# Graph definition
#
#############

def check_input_format(input_data, way):
    if type(input_data) != dict:
        raise ValueError("Input data must be a dictionary")

    communities          = input_data.get('communities')
    communities_names    = input_data.get('communities_names')
    delta                = input_data.get('delta')
    alpha                = input_data.get('alpha')
    beta                 = input_data.get('beta')
    avg_deg              = input_data.get('avg_deg')
    degrees              = input_data.get('degrees')
    xmin                 = input_data.get('xmin')
    input_filename       = input_data.get('input_filename')
    fitness_filename     = input_data.get('fitness_filename')
    theta_filename       = input_data.get('theta_filename')
    probability_filename = input_data.get('probability_filename')
    output_directory     = input_data.get('output_directory')
    n_tests              = input_data.get('n_tests')
    verbose              = input_data.get('verbose')
    save_timestamp       = input_data.get('save_timestamp')

# Concistency checks and data preparation
    if way == 'experiments':
        # Check that the input data is in the right format
        if type(communities) is not dict:
            raise ValueError("Communities must be a dictionary")
        if type(communities_names) is not list:
            input_data['communities_names'] = list(communities.keys())
        if type(delta) is not list:
            raise ValueError("delta must be a list")
        if type(beta) is not float:
            if verbose: print_time(f"Warning beta not set, using default value {BETA_DEFAULT}")
            input_data['beta'] = BETA_DEFAULT
        N = get_N(communities)
        if bool(degrees) == (bool(avg_deg) and bool(alpha)):
            raise ValueError("Either degrees or avg_deg and alpha must be set")
        # Check on degrees
        if bool(degrees):
            if len(degrees) != N:
                raise ValueError("degrees must be a vector of length N")
            else:
                input_data['degrees'] = np.array(degrees)
                input_data['avg_deg'] = np.mean(degrees)
                input_data['alpha'] = None
        else: 
        # Check on avg_deg and alpha
            if type(alpha) is not float:
                raise ValueError("alpha must be a float")
            if type(avg_deg) is not float:
                raise ValueError("avg_deg must be a float")
        if (type(xmin) is float or xmin is None) is False:
            raise ValueError("xmin must be a float or None")
        else:
            input_data['xmin'] = xmin
        ## Convert to numpy for faster computation
        input_data['delta'] = delta = np.array(delta)    
        check_data_concistencies(communities, delta)
    elif way == 'measurements':
        pass
    else:
        raise ValueError("way must be either 'experiments' or 'measurements'")
# Set default values
    if bool(verbose):
        print_time("Verbosity activated")
    else:
        input_data['verbose'] = False
    if type(input_filename) is not str:
        if verbose: print_time(f"Warning fitness_filename not set, using default value {INPUT_FILENAME_DEFAULT}")
        input_data['input_filename'] = INPUT_FILENAME_DEFAULT
    if type(fitness_filename) is not str:
        if verbose: print_time(f"Warning fitness_filename not set, using default value {FITNESS_FILENAME_DEFAULT}")
        input_data['fitness_filename'] = FITNESS_FILENAME_DEFAULT
    if type(theta_filename) is not str:
        if verbose: print_time(f"Warning theta_filename not set, using default value {THETA_FILENAME_DEFAULT}")
        input_data['theta_filename'] = THETA_FILENAME_DEFAULT
    if type(probability_filename) is not str:
        if verbose: print_time(f"Warning probability_filename not set, using default value {PROBABILITY_FILENAME_DEFAULT}")
        input_data['probability_filename'] = PROBABILITY_FILENAME_DEFAULT
    if type(output_directory) is not str:
        if verbose: print_time(f"Warning probability_filename not set, using default value {FOLDER_DEFAULT}")
        input_data['output_directory'] = FOLDER_DEFAULT
    if bool(save_timestamp):
        out_folder = datetime.now().strftime("%Y%m%d-%H%M%S")
        input_data['output_directory'] = os.path.join(input_data['output_directory'], out_folder)
    if (type(n_tests) is int or n_tests is None) is False:
        raise ValueError (f"Warning n_tests must be an integer or None")
    
    return input_data

def get_community_from_index(index, communities):
    for community in communities.keys():
        if index in communities[community]:
            return community

def sample_graph_from_formatted_input(input_data):
    communities          = input_data['communities']
    communities_names    = input_data['communities_names']
    delta                = input_data['delta']
    alpha                = input_data['alpha']
    beta                 = input_data['beta']
    avg_deg              = input_data['avg_deg']
    degrees              = input_data['degrees']
    xmin                 = input_data['xmin']
    verbose              = input_data['verbose']

# Expected and data preparation
    N, E, K = expected_measures(communities, delta, avg_deg)
    n_communities = len(communities_names)
    node_to_communities_int = get_node_index_to_community(N, communities, communities_names)

    if verbose:
        print_time("Expected measures with communities")
        print_time(f"N: {N}", begin = '\t')
        print_time(f"expected E: {E}", begin = '\t')
        print_time(f"K", begin = '\t')
        print_time(K, begin = '\t')

# Initialization
    fitness = get_fitness(N, alpha, avg_deg, degrees, xmin)
    theta = sample_angle(N)

# Get data from the blocks
    sum_block_fitness = np.zeros(n_communities)

    for i_N_b, community in enumerate(communities.keys()):
        community_indices = communities[community]
        sum_block_fitness[i_N_b] = np.sum(fitness[community_indices])

    p_ij = get_p_ij_vectorized(fitness, theta, beta, N, K, sum_block_fitness, node_to_communities_int)
    return  fitness, theta, p_ij