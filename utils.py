import numpy as np
from numpy.random import default_rng
from scipy.stats import lognorm, pareto, expon
AVG_FIT = 10

def get_fitness(fitness_func='one', N=1, rng=None, **params):
    """
    Generate the fitness score for N individuals from the given probability distribution
        
    :param str fitness_func: how to generate the types; can be 'one' (constant f), 'uniform' (uniform f in (0,1)), 'lognormal', 'pareto' or 'truncated_pareto'; the parameters for lognormal and pareto distributions have been selected experimentally, you might want to adjust them; truncated_pareto is truncated at 100, you might want to change this as well
    :param int N: the number of individuals in the population
    :param rng: random number generator to be used
    :type rng: Generator or int or None
    :return: a numpy array of N floats representing the N requested fitness scores
    """

    rng = default_rng(rng)
    if fitness_func == 'one':
        return np.ones(N)
    elif fitness_func == 'uniform':
        return rng.uniform(0,1,N)
    elif 'lognormal' in fitness_func:
        s = params.get('s',1)
        loc = params.get('loc',0)
        scale = params.get('scale',1)
        r = lognorm.rvs(s, loc=loc, scale=scale, size=N)
        #sigma = np.sqrt(2*np.log(avg_fitness)) ## mean = scale*exp(sigma^2/2), scale=1 by default
        #r = lognorm.rvs(sigma, size=N)
    elif 'pareto' in fitness_func:
        # b=3.7
        # loc=-6
        # scale=10
        # r = pareto.rvs(b, loc=loc, scale=scale, size=N)
        # b = avg_fitness/(avg_fitness-1) ## mean = b/(b-1)
        b = params.get('b',1.5)
        loc = params.get('loc',0)
        scale = params.get('scale',1)
        r = pareto.rvs(b, loc=loc, scale=scale, size=N)
    elif 'exponential' in fitness_func:
        scale = params.get('scale',AVG_FIT)  ## mean = scale = 1/lambda
        r = expon.rvs(scale=scale, size=N)
    else:
        print("get_fitness: ERROR! unknown function '{}'".format(fitness_func))
        return 111
    if 'truncated' in fitness_func:
        while True:
            to_redraw = r>100
            num_to_redraw = to_redraw.sum()
            if num_to_redraw:
                print("WARNING: {} draws repeated".format(num_to_redraw))
                if 'lognormal' in fitness_func:
                    r_ = lognorm.rvs(sigma, size=num_to_redraw)
                elif 'pareto' in fitness_func:
                    r_ = pareto.rvs(b, size=num_to_redraw)
                elif 'exponential' in fitness_func:
                    r_ = expon.rvs(scale=scale, size=num_to_redraw)
                r[to_redraw] = r_
            else:
                break
        r = r/r.mean()*AVG_FIT
    return r
