### WARNING: The code is under continuous development: some features are currently hard-coded, some may not yet be documented or conversely may have become obsolete. Please contact the corresponding author at s.guarino (at) iac.cnr.it for clarifications or if you encounter any problems!

# <a name="top"></a> Max-Entropy Block-Model generator
This repository contains Python code to generate random networks with a modular structure, according to one of the following max-entropy block-models:
* Degree-Corrected Block-Model (DCBM): the constraints are the expected number of block-to-block connections and the expected the degree sequence
* Degree-Corrected Directed Block-Model (DCDBM): the constraints are the expected number of block-to-block directed connections and the expected the in-degree and out-degree sequences
* Fitness-Corrected Block-Model (FCBM): the constraints are as in the DCBM, but expressed in terms of network density, block-to-block connection densities and vertex-intrinsic fitness
* Fitness-Corrected Directed Block-Model (FCDBM): the constraints are as in the DCDBM, but expressed in terms of network density, block-to-block directed connection densities and vertex-intrinsic in-fitness and out-fitness

The Python code relies on C code (provided in the `solver` folder) to solve the non-linear system of equations that arise in the formalization of the DCBM (`solver/newtonmkl.c`) and of the DCDBM (`solver/newtonmkl_directed.c`).
The C sources can be compiled running `solver/compmkl.sh` and `solver/compmkl_directed.sh`, respectively.
 
We used the desease diffusion process to evaluate the effecctiveness of our model and described some preliminary results in two papers. In [2](#epidemics) we characterize our network model under selected configurations and we show its potential as a building block for the simulation of infections' propagation. In [3](#venues) We focused on the greatest park of the City of Florence, Italy, to provide experimental evidence that our simulator produces contact graphs with unique, realistic features, and that gaining control of the mechanisms that govern interactions at the local scale allows to unveil and possibly control non-trivial aspects of the epidemic.

