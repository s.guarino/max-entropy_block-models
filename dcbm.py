#
#
#    Copyright (C) 2023 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Fabio Saracco
#
#    This is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This software is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with the code.  If not, see <http://www.gnu.org/licenses/>.
#
#

import os
import sys
import ctypes
import pexpect
from   numpy.random import default_rng
import numpy as np
np.set_printoptions(precision=7, suppress=True)
from datetime import datetime
from scipy.stats import lognorm, pareto, expon
import igraph as ig
#####

# Precision and MAX_ITER of the numerical solver
PRECISION = 0.000001
MAX_ITER = 500

def compute_approximate_ys(K, k, block_labels): 
    """
    """
    block_degrees = K.sum(axis=1)
    k = k**2
    sq_degs = np.diag([k[block_labels==i].sum() for i in range(K.shape[0])])
    y = K/(np.outer(block_degrees,block_degrees)-np.diag(sq_degs))
    return y


def compute_approximate_ys_directed(K, k_in, k_out, block_labels): 
    """
    """
    block_degrees_in  = K.sum(axis=0)
    block_degrees_out = K.sum(axis=1)
    k = k_out*k_in
    sq_degs = np.diag([k[block_labels==i].sum() for i in range(K.shape[0])])
    y = K/(np.outer(block_degrees_out,block_degrees_in)-np.diag(sq_degs))
    return y


def run_solver_equal(data):
    bash_command,M,L,f = data
    if L==0:
        return 0
    idxs = np.triu_indices(len(f),1)
    x = L/((f[idxs[0]]*f[idxs[1]]).sum())
    child = pexpect.spawn(bash_command, [str(len(f)), '0', str(M-L), str(x), str(PRECISION)], echo=False)
    child.delaybeforesend = None
    for _f in f:
        child.sendline(f'{_f}')
    z = float(child.read())
    child.close()
    return z


def run_solver_different(data):
    bash_command,M,L,f1,f2 = data
    if L==0:
        return 0
    x = L/(f1.sum()*f2.sum())
    child = pexpect.spawn(bash_command, [str(len(f1)), str(len(f2)), str(M-L), str(x), str(PRECISION)], echo=False)
    child.delaybeforesend = None
    for _f in f1:
        child.sendline(f'{_f}')
    for _f in f2:
        child.sendline(f'{_f}')
    z = float(child.read())
    child.close()
    return z


def run_solver_newton(data):
    bash_command,N,nblocks,x,y,block_pos,k,K = data
    
    child = pexpect.spawn(bash_command, [str(N), str(nblocks), str(PRECISION), str(MAX_ITER)], echo=False, timeout=36000)
    child.delaybeforesend = None
    for i in range(N):
        child.sendline(f'{k[i]} {block_pos[i]} {x[i]}')

    for i in range(len(y)):
        child.sendline(f'{K[i]} {y[i]}')

    z = [float(v) for v in child.readlines()]
    child.close()
    return z


def run_solver_newton_directed(data):
    bash_command,N,nblocks,x_in,x_out,y,block_pos,k_in,k_out,K = data
    
    child = pexpect.spawn(bash_command, [str(N), str(nblocks), str(PRECISION), str(MAX_ITER)], echo=False, timeout=36000)
    child.delaybeforesend = None

    for i in range(N):
        child.sendline(f'{k_in[i]} {k_out[i]} {block_pos[i]} {x_in[i]} {x_out[i]}')

    for i in range(len(y)):
        child.sendline(f'{K[i]} {y[i]}')
    z = [float(v) for v in child.readlines()]
    child.close()
    return z


def plot_degree_distribution_with_fit(data, plot_to=None):

    fig = plt.figure(figsize=(10,8))
    ax = fig.add_subplot(111)
    ax.set_xscale('log')
    ax.set_yscale('log')

    for k,d in data.items():
        #### scatter plot of data distribution ####
        d = np.array(d)
        xdata, ydata = get_dist(d)

        if xdata[0]==0:
            prob0 = '={:.3f})'.format(ydata[0])
        else:
            prob0 = '=0)'

        ax.scatter(xdata, ydata, s=80, alpha=0.4, label=f'{k} (' + r'$\Pr[0]$' + prob0)
        ymindata = min(ydata)

        fit = fit_data(d, discrete=True, compare=True, verbose=False)
        alpha = fit.alpha
        sigma = fit.sigma
        D     = fit.D
        xmin  = fit.xmin
        xmax = xdata[-1]
        scale = sum(ydata[xdata.index(xmin):xdata.index(xmax)])
        xfit, yfit = get_fit_dist(alpha, xmin, xmax, scale)

        xfit = xfit[yfit>=ymindata]
        yfit = yfit[yfit>=ymindata]
        ax.plot(xfit, yfit, '--', label=f'PL fit of {k} (' + r'$\alpha$' +'={:.2f})'.format(alpha))

        print("\n")
        print("-"*80)
        print(f"FIT DATA for {k}:")
        print("-"*80)
        print("alpha = {}, sigma = {}, D = {}".format(alpha, sigma, D))
        print("xmin = {}, xmax = {}".format(xmin, xmax))
        print("-"*80)
        print("\n")

    title = 'degree distribution'
    ax.set_title(title, fontsize=10)
    ax.legend(loc='upper right', prop={'size': 12})
    ax.tick_params(labelsize=18)
    ax.set_ylabel('Probability', labelpad=40, fontsize=20)

    if plot_to is None:
        outfile = 'degree_distribution.png'
    else:
        outfile = plot_to
    fig.savefig(outfile, bbox_inches='tight') #, dpi=150)
    print("plot_distribution_with_fit: plot saved to {}".format(outfile))

    return

###############################################################################
#
#                               GRAPH CONSTRUCTION
#
###############################################################################

def build_DCBM_graph(K, k, block_labels, use_approximation=True, rng=None):
    if not np.allclose(K,K.T):
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The input matrix K is not symmetric! Abort", sys.stderr)
        sys.exit(111)
    nblocks = block_labels.max()+1
    block_degrees = np.asarray([k[block_labels==i].sum() for i in range(nblocks)])
    if not np.allclose(K.sum(axis=1),block_degrees):
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The input matrix K and vector k are not compatible! Abort", sys.stderr)
        sys.exit(111)
    
    # only consider blocks that are actually used and reset labels accordingly 
    used_blocks = np.unique(block_labels).tolist()
    K = K[used_blocks,:][:,used_blocks]
    block_pos = -np.ones(len(block_labels),dtype=int)
    for i,b in enumerate(used_blocks):
        in_b = block_labels==b
        block_pos[in_b] = i
    block_labels = block_pos
    N = len(block_labels)
    nblocks = block_labels.max()+1

    # compute approximate solution
    if use_approximation:
        x = k
        y = compute_approximate_ys(K, k, block_labels)
    else:
        bash_command = 'solver/newtonmkl'  ### versione di massimo presa da email
        ### TODO: verificare che le 2 versioni del solver siano equivalenti e selezionare quella corretta e/o più efficiente ### 
        if not os.path.exists(bash_command):
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - build_FCBM_graph: [ERROR] file {bash_command} not found! Abort", sys.stderr)
            sys.exit(111)
        np.fill_diagonal(K,np.diag(K)/2)
        K = K[np.triu_indices(K.shape[0])]
        L = K.sum()
        x = k/np.sqrt(2*L) 
        y = K/np.sqrt(2*L) 
        data = bash_command,N,nblocks,x,y,block_labels,k,K
        solution = run_solver_newton(data)
        x = solution[:N]
        x = np.asarray(x)
        y = solution[N:]
        y_square = np.zeros((nblocks,nblocks))
        y_square[np.triu_indices(nblocks)] = y
        y = y_square + y_square.T - np.diag(y_square.diagonal())
  
    edges = []
    rng = default_rng(rng)
    for i in range(0, N):
        # compute edge probabilities 
        single_node_b2b_array = y[block_labels[i]][block_labels]
        ep = x[i] * x[i+1:] * single_node_b2b_array[i+1:]
        if not use_approximation:
            ep = ep/(1+ep)
        # extract and store edges
        to_connect = (ep > rng.uniform(0, 1, ep.shape))
        edges.extend([(i,i+1+c) for c in np.where(to_connect)[0].tolist()])

    return ig.Graph(N, edges=edges, directed=False)


def build_DCDBM_graph(K, k_in, k_out, block_labels, use_approximation=True, rng=None):
    nblocks = block_labels.max()+1
    block_degrees_in  = np.asarray([k_in[block_labels==i].sum() for i in range(nblocks)])
    block_degrees_out = np.asarray([k_out[block_labels==i].sum() for i in range(nblocks)])
    if not (np.allclose(K.sum(axis=1),block_degrees_out) and np.allclose(K.sum(axis=0),block_degrees_in)):
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The input matrix K and vector k are not compatible! Abort", sys.stderr)
        sys.exit(111)
    
    # only consider blocks that are actually used and reset labels accordingly 
    used_blocks = np.unique(block_labels).tolist()
    K = K[used_blocks,:][:,used_blocks]
    block_pos = -np.ones(len(block_labels),dtype=int)
    for i,b in enumerate(used_blocks):
        in_b = block_labels==b
        block_pos[in_b] = i
    block_labels = block_pos
    N = len(block_labels)
    nblocks = block_labels.max()+1

    # compute approximate solution
    if use_approximation:
        x_in  = k_in
        x_out = k_out
        y = compute_approximate_ys_directed(K, k_in, k_out, block_labels)
    else:
        bash_command = 'solver/newtonmkl_directed'  ### versione di massimo presa da email
        ### TODO: verificare che le 2 versioni del solver siano equivalenti e selezionare quella corretta e/o più efficiente ### 
        if not os.path.exists(bash_command):
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - build_FCBM_graph: [ERROR] file {bash_command} not found! Abort", sys.stderr)
            sys.exit(111)
        K = K.flatten()
        L = K.sum()
        x_in  = k_in/np.sqrt(2*L)
        x_out = k_out/np.sqrt(2*L)
        y = K/np.sqrt(2*L)
        #### TODO: verify why division by 2 is necessary in the following ####
        data = bash_command,N,nblocks,x_in,x_out,y,block_labels,k_in,k_out,K
        solution = run_solver_newton_directed(data)
        x_in  = solution[:N]
        x_out = solution[N:2*N]
        x_in  = np.asarray(x_in)
        x_out = np.asarray(x_out)
        y = solution[2*N:]
        y = np.asarray(y).reshape((nblocks,nblocks))
  
    edges = []
    rng = default_rng(rng)
    for i in range(0, N):
        # compute edge probabilities 
        single_node_b2b_array = y[block_labels[i]][block_labels]
        ep = np.concatenate([x_out[i] * x_in[:i] * single_node_b2b_array[:i], [0], x_out[i] * x_in[i+1:] * single_node_b2b_array[i+1:]])
        if not use_approximation:
            ep = ep/(1+ep)
        # extract and store edges
        to_connect = (ep > rng.uniform(0, 1, ep.shape))
        edges.extend([(i,c) for c in np.where(to_connect)[0].tolist()])

    return ig.Graph(N, edges=edges, directed=True)


def main():
    
    ### set seed and generator ###
    seed = None
    rng = default_rng(seed)
    ################
    
    ### set params ###
    nblocks = 2 
    nnodes = 5000
    avgdeg = 5
    alpha = 2.5
    n = 1
    directed = True
    print(f'generating {n} instances of the {"directed "*directed}DCBM with the following input parameters:')
    print(f'\t{nnodes} nodes')
    print(f'\t{nblocks} blocks')
    if directed:
        print(f'\taverage in/out-degree {avgdeg}')
        print(f'\tpowerlaw in/out-degree distribution of exponent {alpha}''')
    else:
        print(f'\taverage degree {avgdeg}')
        print(f'\tpowerlaw degree distribution of exponent {alpha}''')
    block_labels = rng.choice(nblocks,nnodes)
    block_labels.sort()
    
    if directed:
        k = pareto.rvs(b=alpha-1,scale=2*avgdeg*(alpha-2)/(alpha-1),size=nnodes).round()
        if k.sum()%2==1:
            k[0] += 1
        G = ig.Graph.Degree_Sequence(k)
        G.to_directed('random')
        k_in  = np.asarray(G.indegree())
        k_out = np.asarray(G.outdegree())
        data = {'indegree':k_in, 'outdegree':k_out}
    else:
        k = pareto.rvs(b=alpha-1,scale=avgdeg*(alpha-2)/(alpha-1),size=nnodes).round()
        if k.sum()%2==1:
            k[0] += 1
        G = ig.Graph.Degree_Sequence(k)
        data = {'degree':k}
    blocks = ig.VertexClustering(G,block_labels)
    block_connections = blocks.cluster_graph(combine_edges=False)
    K = np.array(block_connections.get_adjacency().data)
    print('generated random structure:')
    for i in range(nblocks):
        print(f'number of nodes of type {i}: {(block_labels==i).sum()}')
    print('K matrix:')
    print(K)
    ##################


    if directed:
        data['appr. in'] = []
        data['appr. out'] = []
        print(f'starting the generation of {n} graphs with the sparse graph approximation')
        
        indeg = np.zeros(nnodes)
        outdeg = np.zeros(nnodes)

        for i in range(n):
            ### create graph with approximation ###
            G = build_DCDBM_graph(K, k_in, k_out, block_labels, use_approximation=True, rng=rng)
            
            print(np.mean(G.indegree()))
            print(np.mean(G.outdegree()))

            output_K = np.zeros(K.shape)
            for e in G.es:
                s,t = e.tuple
                output_K[block_labels[s],block_labels[t]] +=1
                output_K[block_labels[t],block_labels[s]] +=1
            data['appr. in'].extend(G.indegree())
            data['appr. out'].extend(G.outdegree())
            #######################################
            
            indeg += np.asarray(G.indegree())
            outdeg += np.asarray(G.outdegree())

        data['exact in'] = []
        data['exact out'] = []
        print(f'starting the generation of {n} graphs with the exact formulation')
        
        indeg = np.zeros(nnodes)
        outdeg = np.zeros(nnodes)

        for i in range(n):
            ### create graph exact ###
            G = build_DCDBM_graph(K, k_in, k_out, block_labels, use_approximation=False, rng=rng)
            
            print(np.mean(G.indegree()))
            print(np.mean(G.outdegree()))

            output_K = np.zeros(K.shape)
            for e in G.es:
                s,t = e.tuple
                output_K[block_labels[s],block_labels[t]] +=1
                output_K[block_labels[t],block_labels[s]] +=1
            data['exact in'].extend(G.indegree())
            data['exact out'].extend(G.outdegree())
            #######################################
            
            indeg += np.asarray(G.indegree())
            outdeg += np.asarray(G.outdegree())


    else:
        data['appr.'] = []
        for i in range(n):
            ### create graph with approximation ###
            G = build_DCBM_graph(K, k, block_labels, use_approximation=True, rng=rng)
            output_K = np.zeros(K.shape)
            for e in G.es:
                s,t = e.tuple
                output_K[block_labels[s],block_labels[t]] +=1
                output_K[block_labels[t],block_labels[s]] +=1
            data['appr.'].extend(G.degree())
            #######################################

        
        data['exact'] = []
        for i in range(n):
            ### create graph with exact solution ###
            G = build_DCBM_graph(K, k, block_labels, use_approximation=False, rng=rng)
            output_K = np.zeros(K.shape)
            for e in G.es:
                s,t = e.tuple
                output_K[block_labels[s],block_labels[t]] +=1
                output_K[block_labels[t],block_labels[s]] +=1
            data['exact'].extend(G.degree())
            ########################################

    print()
    plot_degree_distribution_with_fit(data, 'degree_distribution_comparison.png')
    

if __name__ == "__main__":
    main()





