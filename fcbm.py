#
#
#    Copyright (C) 2023 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Fabio Saracco
#
#    This is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This software is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with the code.  If not, see <http://www.gnu.org/licenses/>.
#
#

import os
import sys
import ctypes
import pexpect
from   numpy.random import default_rng
import numpy as np
np.set_printoptions(precision=7, suppress=True)
from time import time
import pickle
from datetime import datetime
import igraph as ig
#####
from utils import get_fitness


################################################## MACROS ###################################################
##########################
### General parameters ###
##########################
# Precision and maximum number of iterations for the numerical solver: most likely not to be changed
PRECISION = 0.000001
MAX_ITER = 500
# Whether to generated directed or undirected networks
DIRECTED = True
# Whether to use the approximate solution
USE_APPROX = True
# How many instances of the graph to compute
N_GRAPHS = 10
###############################
### Parameters of the model ###
###############################
# Number of nodes -- for the exact model this can't be too large, but we did not check the running times yet
N = 1000
# Average degree MU or density P -- MU is only used if P is None, otherwise MU is ignored
MU = 10
P = None
# Number of blocks
N_BLOCKS = 4
# Number of nodes in each block -- must be a list of length N_BLOCKS whose sum is N
BLOCK_SIZES = [250,250,250,250]
# Block-mixing matrix, as a N_BLOCKS x N_BLOCKS square matrix 
# if directed, this is later normalized to sum to 1; if undirected, this is later made symmetric (DELTA=DELTA+DELTA.T) and normalized to sum to 2 
DELTA = [[2,1,1,1],[1,2,1,1],[1,1,2,1],[1,1,1,2]]
# Distribution of the in- and out-fitness (or just the undirected fitness), specified as a family of distributions and the corresponding parameters
# the main alternatives are pareto and lognormal distributions, check get_fitness in utils.py for more details 
# for the pareto, see scipy.stats.pareto. basically, the probability density function is f(x)=b/x^{-b-1} and you can shift or rescale it with the loc and scale parameters
INFITNESS_FUNC = 'pareto'    
INFITNESS_PARAMS = {'b':1.5, 'loc':0, 'scale':1}
OUTFITNESS_FUNC = 'pareto'    
OUTFITNESS_PARAMS = {'b':1.5, 'loc':0, 'scale':1}
# FITNESS_FUNC = 'pareto'    
# FITNESS_PARAMS = {'b':1.5, 'loc':0, 'scale':1}
# for the lognormal, see scipy.stats.lognorm. basically, the logarithm of this distribution is a normal with mean mu=ln(scale) and stddev sigma=s, and you can shift it with loc
# INFITNESS_FUNC = 'lognormal'
# INFITNESS_PARAMS = {'s':1, 'loc':0, 'scale':1}
# OUTFITNESS_FUNC = 'lognormal'
# OUTFITNESS_PARAMS = {'s':1, 'loc':0, 'scale':1}
# FITNESS_FUNC = 'lognormal'    
# FITNESS_PARAMS = {'s':1, 'loc':0, 'scale':1}
#############################################################################################################


def compute_blockwise_expected_edges(p, N, delta): 
    """
    Compute the expected number of edges connecting each block pair, as a (nblocks,nblocks) array K.
    By definition, K[I,J] = p * binom(N 2) * delta(I,J) 

    :param int p: the density of the network
    :param int N: the number of nodes in the network
    :param array delta: the matrix of shape (nblocks,nblocks) containing the input blockwise edge-densities 
    :return: the numpy array K of shape (nblocks,nblocks) containing the blockwise expected edge-counts 
    """

    K = p*(N*(N-1)/2)*delta
    # np.fill_diagonal(K,np.diag(K)/2)
    return K


def compute_expected_degrees(p, N, delta, fitness, block_labels): 
    """
    Compute the expected degree of each node, as an array k of length N.
    By definition, k[i] = p * binom(N 2) * fitness[i]/sum_{block_labels[u]==block_labels[i]}fitness[u] * sum_J delta(block_labels[i],J) 

    :param int p: the density of the network
    :param int N: the number of nodes in the network
    :param array delta: the matrix of shape (nblocks,nblocks) containing the input blockwise edge-densities 
    :param array fitness: the array of length N containing the input fitness of each node 
    :param array block_labels: the array of length N containing the block to which each node belongs 
    :return: the numpy array k of length N containing the expected degree of each node 
    """

    denoms = np.zeros(delta.shape[0])
    for i in range(block_labels.max()+1):
        in_b = block_labels==i
        f = fitness[in_b]
        denoms[i] = f.sum()
    k = p*(N*(N-1)/2)*(fitness/denoms[block_labels])*(delta.sum(axis=1))[block_labels]
    return k 


def compute_blockwise_expected_edges_directed(p, N, delta): 
    """
    Compute the expected number of edges connecting each block pair, as a (nblocks,nblocks) array K.
    By definition, K[I,J] = p * N * (N-1) * delta(I,J) 

    :param int p: the density of the network
    :param int N: the number of nodes in the network
    :param array delta: the matrix of shape (nblocks,nblocks) containing the input blockwise edge-densities 
    :return: the numpy array K of shape (nblocks,nblocks) containing the blockwise expected edge-counts 
    """

    K = p*N*(N-1)*delta
    # np.fill_diagonal(K,np.diag(K)/2)
    return K


def compute_expected_indegrees(p, N, delta, fitness, block_labels): 
    """
    Compute the expected indegree of each node, as an array k of length N.
    By definition, k^{in}[i] = p * N * (N-1) * fitness^{in}[i]/sum_{block_labels[u]==block_labels[i]}fitness^{in}[u] * sum_J delta(J,block_labels[i]) 

    :param int p: the density of the network
    :param int N: the number of nodes in the network
    :param array delta: the matrix of shape (nblocks,nblocks) containing the input blockwise edge-densities 
    :param array fitness: the array of length N containing the input in-fitness of each node 
    :param array block_labels: the array of length N containing the block to which each node belongs 
    :return: the numpy array k of length N containing the expected degree of each node 
    """

    denoms = np.zeros(delta.shape[0])
    for i in range(block_labels.max()+1):
        in_b = block_labels==i
        f = fitness[in_b]
        denoms[i] = f.sum()
    k = p*N*(N-1)*(fitness/denoms[block_labels])*(delta.sum(axis=0))[block_labels]
    return k 


def compute_expected_outdegrees(p, N, delta, fitness, block_labels): 
    """
    Compute the expected outdegree of each node, as an array k of length N.
    By definition, k^{out}[i] = p * N * (N-1) * fitness^{out}[i]/sum_{block_labels[u]==block_labels[i]}fitness^{in}[u] * sum_J delta(block_labels[i],J) 

    :param int p: the density of the network
    :param int N: the number of nodes in the network
    :param array delta: the matrix of shape (nblocks,nblocks) containing the input blockwise edge-densities 
    :param array fitness: the array of length N containing the input out-fitness of each node 
    :param array block_labels: the array of length N containing the block to which each node belongs 
    :return: the numpy array k of length N containing the expected degree of each node 
    """

    denoms = np.zeros(delta.shape[0])
    for i in range(block_labels.max()+1):
        in_b = block_labels==i
        f = fitness[in_b]
        denoms[i] = f.sum()
    k = p*N*(N-1)*(fitness/denoms[block_labels])*(delta.sum(axis=1))[block_labels]
    return k


def compute_approximate_ys(K, k, block_labels): 
    """
    """
    block_degrees = K.sum(axis=1)
    k = k**2
    sq_degs = np.diag([k[block_labels==i].sum() for i in range(K.shape[0])])
    y = K/(np.outer(block_degrees,block_degrees)-np.diag(sq_degs))
    return y


def compute_approximate_ys_directed(K, k_in, k_out, block_labels): 
    """
    """
    block_degrees_in  = K.sum(axis=0)
    block_degrees_out = K.sum(axis=1)
    k = k_out*k_in
    sq_degs = np.diag([k[block_labels==i].sum() for i in range(K.shape[0])])
    y = K/(np.outer(block_degrees_out,block_degrees_in)-np.diag(sq_degs))
    return y


def run_solver_equal(data):
    bash_command,M,L,f = data
    if L==0:
        return 0
    idxs = np.triu_indices(len(f),1)
    x = L/((f[idxs[0]]*f[idxs[1]]).sum())
    child = pexpect.spawn(bash_command, [str(len(f)), '0', str(M-L), str(x), str(PRECISION)], echo=False)
    child.delaybeforesend = None
    for _f in f:
        child.sendline(f'{_f}')
    z = float(child.read())
    child.close()
    return z


def run_solver_different(data):
    bash_command,M,L,f1,f2 = data
    if L==0:
        return 0
    x = L/(f1.sum()*f2.sum())
    child = pexpect.spawn(bash_command, [str(len(f1)), str(len(f2)), str(M-L), str(x), str(PRECISION)], echo=False)
    child.delaybeforesend = None
    for _f in f1:
        child.sendline(f'{_f}')
    for _f in f2:
        child.sendline(f'{_f}')
    z = float(child.read())
    child.close()
    return z


def run_solver_newton(data):
    bash_command,N,nblocks,x,y,block_pos,k,K = data
    child = pexpect.spawn(bash_command, [str(N), str(nblocks), str(PRECISION), str(MAX_ITER)], echo=False, timeout=36000)
    child.delaybeforesend = None
    for i in range(N):
        child.sendline(f'{k[i]} {block_pos[i]} {x[i]}')
    for i in range(len(y)):
        child.sendline(f'{K[i]} {y[i]}')
    z = [float(v) for v in child.readlines()]
    child.close()
    return z


def run_solver_newton_directed(data):
    bash_command,N,nblocks,x_in,x_out,y,block_pos,k_in,k_out,K = data

    child = pexpect.spawn(bash_command, [str(N), str(nblocks), str(PRECISION), str(MAX_ITER)], echo=False, timeout=36000)
    child.delaybeforesend = None

    for i in range(N):
        child.sendline(f'{k_in[i]} {k_out[i]} {block_pos[i]} {x_in[i]} {x_out[i]}')

    for i in range(len(y)):
        child.sendline(f'{K[i]} {y[i]}')
    z = [float(v) for v in child.readlines()]
    child.close()
    return z


def plot_degree_distribution_with_fit(data, plot_to=None, directed=False):

    fig = plt.figure(figsize=(10,8))
    ax = fig.add_subplot(111)
    ax.set_xscale('log')
    ax.set_yscale('log')

    for k,d in data.items():
        #### scatter plot of data distribution ####
        d = np.array(d)
        xdata, ydata = get_dist(d)

        if xdata[0]==0:
            prob0 = '={:.3f})'.format(ydata[0])
        else:
            prob0 = '=0)'

        ax.scatter(xdata, ydata, s=80, alpha=0.4, label=f'{k} (' + r'$\Pr[0]$' + prob0)
        ymindata = min(ydata)

        fit = fit_data(d, discrete=True, compare=True, verbose=False)
        alpha = fit.alpha
        sigma = fit.sigma
        D     = fit.D
        xmin  = fit.xmin
        xmax = xdata[-1]
        scale = sum(ydata[xdata.index(xmin):xdata.index(xmax)])
        xfit, yfit = get_fit_dist(alpha, xmin, xmax, scale)

        # ymindata = min(ymindata, yminfib)
        xfit = xfit[yfit>=ymindata]
        yfit = yfit[yfit>=ymindata]
        ax.plot(xfit, yfit, '--', label=f'PL fit of {k} (' + r'$\alpha$' +'={:.2f})'.format(alpha))

        print("\n")
        print("-"*80)
        print(f"FIT DATA for {k}:")
        print("-"*80)
        print("alpha = {}, sigma = {}, D = {}".format(alpha, sigma, D))
        print("xmin = {}, xmax = {}".format(xmin, xmax))
        #print("yfit: ", yfit)
        #print("ylim bottom = ", ylimbottom)
        print("-"*80)
        print("\n")

    title = 'degree distribution'
    ax.set_title(title, fontsize=10)
    ax.legend(loc='upper right', prop={'size': 12})
    ax.tick_params(labelsize=18)
    ax.set_ylabel('Probability', labelpad=40, fontsize=20)

    if plot_to is None:
        outfile = 'degree_distribution.png'
    else:
        outfile = plot_to
    fig.savefig(outfile, bbox_inches='tight') #, dpi=150)
    print("plot_degree_distribution_with_fit: plot saved to {}".format(outfile))

    return

###############################################################################
#
#                               GRAPH CONSTRUCTION
#
###############################################################################

def build_FCBM_graph(p, delta, fitness, block_labels, use_approximation=True, rng=None):
    if not (np.isclose(delta.sum(),2) and np.allclose(delta, delta.T)):
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The input matrix delta is not symmetric and/or does not sum to 2! Abort", sys.stderr)
        sys.exit(111)
    if not 0<p<1:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The input density p is not in (0,1)! Abort", sys.stderr)
        sys.exit(111)
    
    # only consider blocks that are actually used and reset labels accordingly 
    used_blocks = np.unique(block_labels).tolist()
    delta = delta[used_blocks,:][:,used_blocks]
    block_pos = -np.ones(len(block_labels),dtype=int)
    for i,b in enumerate(used_blocks):
        in_b = block_labels==b
        block_pos[in_b] = i
    block_labels = block_pos
    N = len(block_labels)
    nblocks = block_labels.max()+1

    k = compute_expected_degrees(p, N, delta, fitness, block_labels)
    K = compute_blockwise_expected_edges(p, N, delta) 
    
    # compute approximate solution
    if use_approximation:
        x = k
        y = compute_approximate_ys(K, k, block_labels)
    else:
        bash_command = 'solver/newtonmkl'  ### versione di massimo presa da email
        if not os.path.exists(bash_command):
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - build_FCBM_graph: [ERROR] file {bash_command} not found! Abort", sys.stderr)
            sys.exit(111)
        np.fill_diagonal(K,np.diag(K)/2)
        K = K[np.triu_indices(K.shape[0])]
        L = K.sum()
        x = k/np.sqrt(2*L) 
        y = K/np.sqrt(2*L) 
        data = bash_command,N,nblocks,x,y,block_labels,k,K
        solution = run_solver_newton(data)
        x = solution[:N]
        x = np.asarray(x)
        y = solution[N:]
        y_square = np.zeros((nblocks,nblocks))
        y_square[np.triu_indices(nblocks)] = y
        y = y_square + y_square.T - np.diag(y_square.diagonal())
  
    edges = []
    rng = default_rng(rng)
    for i in range(0, N):
        # compute edge probabilities 
        single_node_b2b_array = y[block_labels[i]][block_labels]
        ep = x[i] * x[i+1:] * single_node_b2b_array[i+1:]
        if not use_approximation:
            ep = ep/(1+ep)
        # extract and store edges
        to_connect = (ep > rng.uniform(0, 1, ep.shape))
        edges.extend([(i,i+1+c) for c in np.where(to_connect)[0].tolist()])
    
    return ig.Graph(N, edges=edges, directed=False)


def build_FCDBM_graph(p, delta, fitness_in, fitness_out, block_labels, use_approximation=True, rng=None):
    if not np.isclose(delta.sum(),1):
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The input matrix delta does not sum to 1! Abort", sys.stderr)
        sys.exit(111)
    if not 0<p<1:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The input density p is not in (0,1)! Abort", sys.stderr)
        sys.exit(111)
    
    # only consider blocks that are actually used and reset labels accordingly 
    used_blocks = np.unique(block_labels).tolist()
    delta = delta[used_blocks,:][:,used_blocks]
    block_pos = -np.ones(len(block_labels),dtype=int)
    for i,b in enumerate(used_blocks):
        in_b = block_labels==b
        block_pos[in_b] = i
    block_labels = block_pos
    N = len(block_labels)
    nblocks = block_labels.max()+1

    k_in  = compute_expected_indegrees(p, N, delta, fitness_in, block_labels)
    k_out = compute_expected_outdegrees(p, N, delta, fitness_out, block_labels)
    K = compute_blockwise_expected_edges_directed(p, N, delta) 

    # compute approximate solution
    if use_approximation:
        x_in  = k_in
        x_out = k_out
        y = compute_approximate_ys_directed(K, k_in, k_out, block_labels)
    else:
        bash_command = 'solver/newtonmkl_directed'  ### versione di massimo presa da email
        if not os.path.exists(bash_command):
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - build_FCBM_graph: [ERROR] file {bash_command} not found! Abort", sys.stderr)
            sys.exit(111)
        K = K.flatten()
        L = K.sum()
        x_in  = k_in/np.sqrt(2*L)
        x_out = k_out/np.sqrt(2*L)
        y = K/np.sqrt(2*L)
        data = bash_command,N,nblocks,x_in,x_out,y,block_labels,k_in,k_out,K
        solution = run_solver_newton_directed(data)
        x_in  = solution[:N]
        x_out = solution[N:2*N]
        x_in  = np.asarray(x_in)
        x_out = np.asarray(x_out)
        y = solution[2*N:]
        y = np.asarray(y).reshape((nblocks,nblocks))
  
    edges = []
    rng = default_rng(rng)
    for i in range(0, N):
        # compute edge probabilities 
        single_node_b2b_array = y[block_labels[i]][block_labels]
        ep = np.concatenate([x_out[i] * x_in[:i] * single_node_b2b_array[:i], [0], x_out[i] * x_in[i+1:] * single_node_b2b_array[i+1:]])
        if not use_approximation:
            ep = ep/(1+ep)
        # extract and store edges
        to_connect = (ep > rng.uniform(0, 1, ep.shape))
        edges.extend([(i,c) for c in np.where(to_connect)[0].tolist()])

    return ig.Graph(N, edges=edges, directed=True)


def main():
    
    ### set seed and generator ###
    # seed = 1
    seed = None
    rng = default_rng(seed)
    ################
    
    ### set params ###
    nblocks = N_BLOCKS 
    nnodes = N
    if P is None:
        mu = MU
        p = mu/(nnodes-1)
    else:
        p = P
        mu = p*(nnodes-1)
    n = N_GRAPHS
    directed = DIRECTED
    print(f'''generating {n} instances of the {"directed "*directed} FCBM having:
            {nnodes} nodes
            {nblocks} blocks
            density {round(p,3)} (average degree: {mu})
            the model is initialized with the following parameters:''')
    block_labels = np.zeros(nnodes, dtype=int)
    for i in range(1,nblocks):
        block_labels[sum(BLOCK_SIZES[:i]):] += 1
    for i in range(nblocks):
        print(f'number of nodes of type {i}: {(block_labels==i).sum()}')
    delta = np.asarray(DELTA)
    if directed:
        delta = delta/delta.sum()
    else:
        delta = delta+delta.T
        delta = 2*delta/delta.sum()
    print('delta matrix:')
    print(delta.round(3))
    if directed: 
        infitness_func = INFITNESS_FUNC
        infitness_params = INFITNESS_PARAMS
        outfitness_func = OUTFITNESS_FUNC
        outfitness_params = OUTFITNESS_PARAMS
        fitness_in = get_fitness(infitness_func, nnodes, **infitness_params)
        print(f'{infitness_func} in-fitness generated')
        fitness_out = get_fitness(outfitness_func, nnodes, **outfitness_params)
        print(f'{outfitness_func} out-fitness generated')
        data = {'fitness_in':(fitness_in/fitness_in.mean()*mu).round(), 'fitness_out':(fitness_out/fitness_out.mean()*mu).round()}
    else: 
        fitness_func = FITNESS_FUNC
        fitness_params = FITNESS_PARAMS
        fitness = get_fitness(fitness_func, nnodes, **fitness_params)
        print(f'{fitness_func} fitness generated')
        data = {'fitness':(fitness/fitness.mean()*mu).round()}
   
    graphs = {}

    if directed:
        if USE_APPROX:

            data['appr. in'] = []
            data['appr. out'] = []
            graphs['appr.'] = [] 
            print(f'starting the generation of {n} directed graphs with the sparse graph approximation')
            
            avg_delta = np.zeros(delta.shape)

            times = []
            i = 0
            while i<n:
                try:
                    st = time() 
                    ### create graph with approximation ###
                    G = build_FCDBM_graph(p, delta, fitness_in, fitness_out, block_labels, use_approximation=True, rng=rng)
                    graphs['appr.'].append(G)
                    et = time()
                    times.append(et-st)
                    output_delta = np.zeros(delta.shape)
                    for e in G.es:
                        s,t = e.tuple
                        output_delta[block_labels[s],block_labels[t]] +=1
                    output_delta /= G.ecount()
                    data['appr. in'].extend(G.indegree())
                    data['appr. out'].extend(G.outdegree())
                    #######################################
                    
                    avg_delta += output_delta
                    print(f'graph {i} generated!')
                    print(f'time to create graph {i}: {et-st} secs')
                    i += 1
                    graphs_fname = f'graphs_appr_FCBM_directed.pickle'
                    with open(graphs_fname, 'wb') as f:
                        pickle.dump(graphs['appr.'], f)
                except:
                    print('graph generation failed! retrying...')
    
            print('all appr. graphs generated!')
            print(f'average time to create a graph: {np.mean(times)} secs')

            print('\naverage output delta matrix:')
            print((avg_delta/n).round(3))
            
            #############################################
        
        else:

            data['exact in'] = []
            data['exact out'] = []
            graphs['exact'] = [] 
            print(f'starting the generation of {n} directed graphs with the exact formulation')
            
            avg_delta = np.zeros(delta.shape)

            times = []
            i = 0
            while i<n:
                st = time() 
                ### create graph with approximation ###
                try:
                    G = build_FCDBM_graph(p, delta, fitness_in, fitness_out, block_labels, use_approximation=False, rng=rng)
                    graphs['exact'].append(G)
                    et = time()
                    times.append(et-st)
                    output_delta = np.zeros(delta.shape)
                    for e in G.es:
                        s,t = e.tuple
                        output_delta[block_labels[s],block_labels[t]] +=1
                    output_delta /= G.ecount()
                    data['exact in'].extend(G.indegree())
                    data['exact out'].extend(G.outdegree())
                    #######################################
                    
                    avg_delta += output_delta
                    print(f'graph {i} generated!')
                    print(f'time to create graph {i}: {et-st} secs')
                    i += 1
                    graphs_fname = f'graphs_exact_FCBM_directed.pickle'
                    with open(graphs_fname, 'wb') as f:
                        pickle.dump(graphs['exact'], f)
                except:
                    print('graph generation failed! retrying...')
        
            print('all exact graphs generated!')
            print(f'average time to create a graph: {np.mean(times)} secs')

            print('\naverage output delta matrix:')
            print((avg_delta/n).round(3))
        
            #############################################

    else:
        if USE_APPROX:
        
            data['appr.'] = []
            print(f'starting the generation of {n} graphs with the sparse graph approximation')
            
            avg_delta = np.zeros(delta.shape)
            for i in range(n):
                ### create graph with approximation ###
                G = build_FCBM_graph(p, delta, fitness, block_labels, use_approximation=True, rng=rng)
                output_delta = np.zeros(delta.shape)
                for e in G.es:
                    s,t = e.tuple
                    output_delta[block_labels[s],block_labels[t]] +=1
                    output_delta[block_labels[t],block_labels[s]] +=1
                output_delta /= G.ecount()
                data['appr.'].extend(G.degree())
                #######################################
                avg_delta += output_delta
            
            print('graphs generated!')
            print('\naverage output delta matrix:')
            print((avg_delta/n).round(3))
       
        else:

            data['exact'] = []
            print(f'starting the generation of {n} graphs with the exact formulation')
            
            avg_delta = np.zeros(delta.shape)
            for i in range(n):
                ### create graph with exact solution ###
                G = build_FCBM_graph(p, delta, fitness, block_labels, use_approximation=False, rng=rng)
                output_delta = np.zeros(delta.shape)
                for e in G.es:
                    s,t = e.tuple
                    output_delta[block_labels[s],block_labels[t]] +=1
                    output_delta[block_labels[t],block_labels[s]] +=1
                output_delta /= G.ecount()
                data['exact'].extend(G.degree())
                ########################################
                avg_delta += output_delta
            
            print('graphs generated!')
            print('\naverage output delta matrix:')
            print((avg_delta/n).round(3))
  
    t = "directed" if directed else "undirected"
    a = "approx" if USE_APPROX else "exact"
    data_fname = f'data_for_plots_FCBM_{n}_graphs_{t}_{a}.pickle'
    with open(data_fname, 'wb') as f:
        pickle.dump(data, f)
    
    graphs_fname = f'graphs_FCBM_{t}_{a}.pickle'
    with open(graphs_fname, 'wb') as f:
        pickle.dump(graphs, f)

if __name__ == "__main__":
    main()





